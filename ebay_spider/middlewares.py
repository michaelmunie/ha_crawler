import random

from pyvirtualdisplay import Display
from scrapy import signals
from scrapy.conf import settings
from scrapy.http import HtmlResponse
from selenium import webdriver


class RandomUserAgentMiddleware(object):
    def process_request(self, request, spider):
        ua = random.choice(settings.get('USER_AGENT_LIST'))
        if ua:
            request.headers.setdefault('User-Agent', ua)
