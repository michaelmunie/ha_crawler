# # -*- coding: utf-8 -*-
#
# # Scrapy settings for ebay_spider project
# #
# # For simplicity, this file contains only settings considered important or
# # commonly used. You can find more settings consulting the documentation:
# #
# #     http://doc.scrapy.org/en/latest/topics/settings.html
# #     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
# #     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#
#
# # All proxies seem real sketch.  Probably best to try a VPN+Tor combo
# # https://luminati.io/#plans
# # proxymesh
# # imacros.net
# # http://stormproxies.com
# # ghostproxies.com
# # https://microleaves.com/
# # IPVPN






# source ~/great_collections_venv/bin/activate

# sudo python /home/ubuntu/ha_crawler/ebay_spider/scripts/download_next_ha_auction.py --last_day_of_auction 19-04-28 --num_coins_for_sale 4004 --magic_auction_number 4294945546


#
import getpass


def am_i_local():
    if getpass.getuser() == 'munie':
        local = True
    elif getpass.getuser() == 'ubuntu':
        local = False
    else:
        raise NotImplementedError
    return local

LOCAL = am_i_local()
print("Am I Local?", LOCAL)
#
BOT_NAME = 'ebay_spider'

SPIDER_MODULES = ['ebay_spider.spiders']
NEWSPIDER_MODULE = 'ebay_spider.spiders'

# # TODO: bit of a messy overlap in logic with IMAGES_STORE.

LOG_STDOUT = False  # If this is true, then scrappy shell doesnt work right.  Can also try scrapy shell --nolog
if LOCAL is True:
    proxy_file = '/home/munie/PycharmProjects/great_collections_coin_crawler/proxy_list.txt'
else:
    proxy_file = '/home/ubuntu/great_collections_coin_crawler/proxy_list.txt'


DOWNLOAD_IMAGES = True
if DOWNLOAD_IMAGES is False:
    for i in range(18):
        print('****************Not downloading images********************8')

MEDIA_ALLOW_REDIRECTS = True
IMAGES_MIN_HEIGHT = 499
IMAGES_MIN_WIDTH = 499
#
#
#
#
GRADING_AGENCIES = ['pcgs', 'ngc', 'anacs', 'icg']
NORMAL_GRADES = ['po', 'p', 'fr', 'f', 'ag', 'g', 'vg', 'g', 'vf', 'xf', 'au', 'ms']
PROOF_GRADES = ['pr', 'pf', 'proof']

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ebay_spider (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False



# Disable cookies (enabled by default)
COOKIES_ENABLED = False
#
# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False
#
# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Language': 'en',
}

### More comprehensive list can be found at
### http://techpatterns.com/forums/about304.html
# https://techblog.willshouse.com/2012/01/03/most-common-user-agents/
USER_AGENT_LIST = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15',
'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0',
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134',
'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',]


# Polipo port on localhost.
# See https://pkmishra.github.io/blog/2013/03/18/how-to-run-scrapy-with-TOR-and-multiple-browser-agents-part-1-mac/
# If polipo not running:
# sudo polipo
# To check if Tor is running
# curl --socks5 localhost:9050 --socks5-hostname localhost:9050 -s https://check.torproject.org/ | cat | grep -m 1 Congratulations | xargs
# if LOCAL is False:
#     HTTP_PROXY = 'http://127.0.0.1:8123'


# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'ebay_spider.middlewares.MyCustomSpiderMiddleware': 543,
#}

# RETRY_TIMES = 3
# RETRY_HTTP_CODES = [500, 502, 503, 504, 408, 400]
# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html







#
# CONCURRENT_REQUESTS = 50
# CONCURRENT_REQUESTS_PER_DOMAIN = 50
proxy_choices = [
                 'us-ca.proxymesh.com:31280',
                 'us.proxymesh.com:31280',
                 'us-dc.proxymesh.com:31280',
                 'us-ny.proxymesh.com:31280',
                 'uk.proxymesh.com:31280',
                 ]
PROXIES = list()
for p in proxy_choices:
    # Since I'm leaving on username and password, need to use ip auth
    PROXIES.append({"https": "https://{}/".format(p),
                    "http": "http://{}/".format(p)})



# Retry many times since proxies often fail
RETRY_TIMES = 3
# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408, 523]

DOWNLOADER_MIDDLEWARES = {
    'ebay_spider.middlewares.RandomUserAgentMiddleware': 80,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    'scrapy_proxies.RandomProxy': 100,
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
    # 'ebay_spider.middlewares.SeleniumClickerMiddleware': 420
}

PROXY_LIST = proxy_file
# have to update or rewrite proxy library im using
PROXY_MODE = 0

AUTOTHROTTLE_ENABLED = True
# The initial download delay
AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
AUTOTHROTTLE_MAX_DELAY = 1500
DOWNLOAD_DELAY = 3.0
# The average number of requests Scrapy should be sending in parallel to
# each remote server
AUTOTHROTTLE_TARGET_CONCURRENCY = 0.5
# Enable showing throttling stats for every response received:
AUTOTHROTTLE_DEBUG = True
    #


CLOSESPIDER_ITEMCOUNT = 8000
CLOSESPIDER_ERRORCOUNT = 500


EXTENSIONS = {
    'scrapy.extensions.logstats.LogStats': 500,
    'scrapy.extensions.closespider.CloseSpider': 500,
}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'ebay_spider.pipelines.EbaySpiderPipeline': 300,
}

