# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Identity, TakeFirst, MapCompose, Join 
#import urlparlse
# from w3lib.html import remove_tags


class CoinItem(scrapy.Item):
    source = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: coins.ha.com
    ngc_id = scrapy.Field(        
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 22EL
    pcgs_id = scrapy.Field(        
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 2746
    description = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 1946-D 1C MS67 Red NGC.
    grading_agency = scrapy.Field(
        input_processor=Identity(),
        output_processor=Join(),
    ) #ex: in ['PCGS', 'NGC', 'ANACS', 'ICG']
    great_collections_id = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 1234568
    grade = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 54
    details = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: True
    plus = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: True
    cac = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: True
    auction_date = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: True
    certification_number = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: 4564757864156
    proof = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #ex: False
    price = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: 5000
    image_urls = scrapy.Field()
    images = scrapy.Field()
    
    uuid = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )
    coin_url = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    ) #http://gc.com/coin/3424/





# Evaluation like HiNet
# weighted sum of crossentropy loss on [coin_type, sub_coin_type, sub_sub_coin_type, year, pcgs_id, sub_pcgs_id]
# Try training with early weights high for coin_type, and ending weights high for pcgs_id, year, and sub_pcgs_id
# Also track accuracy for pcgs_id, year, and sub_pcgs_id


class PCGSIDItem(scrapy.Item):
    description = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: 1946-D 1C MS67 Red NGC.
    coin_type = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  #  Half-Cents and Cents
    sub_coin_type = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  #  Indian Cent (1859 - 1909)
    sub_sub_coin_type = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # Type 1, No Shield, MS

    pcgs_id = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: 1052
    sub_pcgs_id = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: Base or 35120


    population_list = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: 2 	5 	33 	16 	18 	10 	15 	11 	16 	0 0	1 	14 	11 	11  9
    # Sum plus types into overall or just skip whatever is easiest
    population_key = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    #     Skip + types
    )  # ex: 'VG  F 	VF 	40 45 50 	 53 	55 	58 	60 	61 	62 	63 	64 	65 	66 	67 	68 	69 	70'


    proof = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: False
    year = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )  # ex: 1982
    uuid = scrapy.Field(
        input_processor=Identity(),
        output_processor=TakeFirst(),
    )
