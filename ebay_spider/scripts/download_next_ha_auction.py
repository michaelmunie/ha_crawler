#!/home/munie/great_collections_venv/bin/python

import subprocess
import datetime
import argparse


def subprocess_cmd(command):
    process = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True)
    process_stdout = process.communicate()[0].strip()
    print(process_stdout)


parser = argparse.ArgumentParser(description='Script to download files for ha auction')
parser.add_argument('--last_day_of_auction', action="store", dest='last_day_of_auction', default='')
parser.add_argument('--num_coins_for_sale', action="store", dest='num_coins_for_sale', default='')
parser.add_argument('--magic_auction_number', action="store", dest='magic_auction_number', default='')

args = parser.parse_args()

magic_auction_number = args.magic_auction_number
last_day_of_auction = args.last_day_of_auction
num_coins_for_sale = args.num_coins_for_sale

auction_date = datetime.datetime.strptime(last_day_of_auction, '%y-%m-%d')

ha_website_date = auction_date.strftime('%Y-%m-%d')
my_format_date = auction_date.strftime('%y_%m_%d')
my_folder_format_date = auction_date.strftime('%Y_%m_%d')


download_location = "/home/munie/work/data/raw/ha/%s_auction/download/" % my_folder_format_date
cropped_location = "/home/munie/work/data/raw/ha/%s_auction/cropped/" % my_folder_format_date
resized_location = "/home/munie/work/data/raw/ha/%s_auction/resized/" % my_folder_format_date

subprocess.check_call(['mkdir', '-p', download_location])
subprocess.check_call(['mkdir', '-p', cropped_location])
subprocess.check_call(['mkdir', '-p', resized_location])

download_location_details = "/home/munie/work/data/raw/ha/%s_auction_details/download/" % my_folder_format_date
cropped_location_details = "/home/munie/work/data/raw/ha/%s_auction_details/cropped/" % my_folder_format_date
resized_location_details = "/home/munie/work/data/raw/ha/%s_auction_details/resized/" % my_folder_format_date

subprocess.check_call(['mkdir', '-p', download_location_details])
subprocess.check_call(['mkdir', '-p', cropped_location_details])
subprocess.check_call(['mkdir', '-p', resized_location_details])

my_coin_jl_file = '/home/munie/work/ha_crawler/auction_ha_%s.jl' % my_format_date
print(['scrapy', 'crawl', 'ha_coins_buy',
                       '-a', 'magic_auction_number=%s' % magic_auction_number,
                       '-a', 'num_coins_for_sale=%s' % num_coins_for_sale,
                       '-a', 'download_location=%s' % download_location,
                       '-o', my_coin_jl_file])
subprocess.check_call(['scrapy', 'crawl', 'ha_coins_buy',
                       '-a', 'magic_auction_number=%s' % magic_auction_number,
                       '-a', 'num_coins_for_sale=%s' % num_coins_for_sale,
                       '-a', 'download_location=%s' % download_location,
                       '-o', my_coin_jl_file])

cmd = 'cd %s; cp *.details %s' % (download_location, download_location_details)
subprocess_cmd(cmd)

# TODO: make sure this command works
cmd = "cd %s; find . -type f -name '*.jpeg.details' -print0 | xargs -0 rename 's/\.jpeg.details/\.jpeg/'" % download_location_details
subprocess_cmd(cmd)




