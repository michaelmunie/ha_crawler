import pickle

with open('unique_coins_dict.pickle', 'rb') as f:
    a = pickle.load(f)

error_pcgs = list()
error_ngc = list()
total_error = 0
ngs_pcgs_mapping = dict()
pcgs_ids = set()

for v in a.values():
    pcgs_id = v.get('pcgs_id', None)
    ngc_id = v.get('ngc_id', None)

    if pcgs_id is None and ngc_id is None:
        total_error +=1
    elif pcgs_id is None:
        error_ngc.append(ngc_id)
    elif ngc_id is None:
        error_pcgs.append(pcgs_id)
    elif ngc_id in ngs_pcgs_mapping:
        ngs_pcgs_mapping[ngc_id].add(pcgs_id)
        pcgs_ids.add(pcgs_id)
    else:
        ngs_pcgs_mapping[ngc_id] = {pcgs_id}
        pcgs_ids.add(pcgs_id)

print(len(ngs_pcgs_mapping))
print(len(error_pcgs))
print(len(error_ngc))
print(total_error)

print(len(pcgs_ids))
