import boto3
import random
import csv

# IMAGES_STORE = 's3://numentio.coins/'
# AWS_FOLDER = 'silver_and_related_dollars'
# prefix = 'silver_and_related_dollars/full/'
# NUMBER_PER_BUCKET = 200
TO_TAKE_FRACTION = 0.005
# 'dimes', 'double_eagles', 'early_eagles',
PREFIXES = ['half_dollars', 'large_cents', 'nickels', 'quarter_eagles',
            'quarters', 'silver_and_related_dollars', 'small_eagles']

# prefix = 'quarter_eagles/full/'

s3 = boto3.resource('s3')
s3client = boto3.client('s3')
COINS_BUCKET_NAME= 'numentio.coins'

CROWDFLOWER_BUCKET_NAME = 'ncrowdflower'


# result = s3client.list_objects(Bucket=bucket, Prefix=prefix)
# for object in result["Contents"]:
#     print(object["Key"])
#     s3.Bucket(bucket).download_file(object["Key"], '/home/munie/Desktop/' + object["Key"].split('/')[-1])
#     break


def get_all_s3_keys(bucket, prefix, client):
    """Get a list of all keys in an S3 bucket."""
    keys = []

    kwargs = {'Bucket': bucket, 'Prefix': prefix}
    while True:
        resp = client.list_objects_v2(**kwargs)
        for obj in resp['Contents']:
            keys.append(obj['Key'])

        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break

    return keys


for p in PREFIXES:
    print('Getting items in bucket:', p)
    prefix = p + '/full/'
    all_keys = get_all_s3_keys(COINS_BUCKET_NAME, prefix, s3client)
    selected_keys = random.sample(all_keys, int(TO_TAKE_FRACTION * len(all_keys)))
    coins_bucket = s3.Bucket(COINS_BUCKET_NAME)
    print('Getting', len(selected_keys), 'keys')
    for key in selected_keys:
        filename = key.split('/')[-1]
        coins_bucket.download_file(key, '/home/munie/Desktop/crowdflower/' + filename)
        # s3.Object(crowdflower_bucket, filename).copy_from(CopySource=bucket + '/' + key)
        # crowdflower_bucket.upload_file, filename).copy_from(CopySource=coins_bucket_name + '/' + key)
        field = 'https://s3.amazonaws.com/' + CROWDFLOWER_BUCKET_NAME + '/' + filename
        copy_source = {
            'Bucket': COINS_BUCKET_NAME,
            'Key': key
        }
        s3.meta.client.copy(copy_source, CROWDFLOWER_BUCKET_NAME, filename)

        fields = [field, ]
        with open(r'/home/munie/Desktop/crowdflower/crowdflower_to_label.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(fields)








# for o in result.get('CommonPrefixes'):
#     print('sub folder : ', o.get('Prefix'))


# theobjects = s3client.list_objects_v2(Bucket='numentio.coins')
# for object in theobjects["Contents"]:
#     print(object["Key"])


