import re
import urllib.parse as urlparse
import uuid
import random
from io import BytesIO

# import pytesseract
import requests
import scrapy
from PIL import Image
from scrapy.conf import settings
from scrapy.loader import ItemLoader
from w3lib.html import remove_tags
import urllib3
import math

from ebay_spider.items import CoinItem


# Run with:
# pip install -r requirements.txt
# scrapy crawl gc_coins -o auction_18_11_11.jl
#  -s JOBDIR=/home/munie/gc_coins-18_09_02-001

# git fetch --all
# git reset --hard origin/master

# scrapy crawl gc_coins -o auction_18_09_09.jl
# Resume with same command


class DLHACoinsBuySpider(scrapy.Spider):
    name = 'ha_coins_buy'
    NORMAL_GRADES = settings.get('NORMAL_GRADES')
    GRADING_AGENCIES = settings.get('GRADING_AGENCIES')
    PROOF_GRADES = settings.get('PROOF_GRADES')


    def __init__(self, magic_auction_number="",
                 download_location="",
                 num_coins_for_sale="",
                 *args, **kwargs):
        super(DLHACoinsBuySpider, self).__init__(*args, **kwargs)
        self.magic_auction_number = magic_auction_number
        self.download_location = download_location
        self.num_coins_for_sale = int(num_coins_for_sale)

    def start_requests(self):
        yield scrapy.Request(url='https://www.whatismyip.com/',
                             callback=self.check_for_proxy)
        a = 'https://coins.ha.com/c/search-results.zx?No='
        b = '&N=51+793+794+791+1577+792+2088+' + self.magic_auction_number

        all_coins_to_buy = [a + str(i) + b for i in range(0, self.num_coins_for_sale, 25)]
        for url in all_coins_to_buy:
            yield scrapy.Request(url=url, callback=self.parse_search_page)

    def check_for_proxy(self, response):
        items = response.selector.xpath('//li[contains(@class, "list-group-item")]/text()').extract()
        print(items)
        for i in items:
            bad_words = ['naperville', 'woodridge', 'lisle', 'il us']
            for fuck in bad_words:
                if fuck in i:
                    for i in range(10000):
                        print("You're blowing it!  Cobb salad?  It's a frickin' Waldorf.")


    def parse_search_page(self, response):
        items = response.selector.xpath(
            '//li[contains(concat(" ", normalize-space(div/div/a/@class)," "), " item-title ")]')

        for item in items:
            item_page = \
                item.xpath('div/div/a[contains(concat(" ", normalize-space(@class)," "), " item-title ")]/@href')\
                .extract_first()
            ga = item.xpath('div/div/div/div[contains(span/., "Service")]/strong/text()').extract_first()
            grade = item.xpath('div/div/div/div[contains(span/., "Grade")]/strong/text()').extract_first()
            print("parsing pages ", item_page, "grade", grade, "agency", ga)
            yield scrapy.Request(url=item_page, callback=self.parse_item_page, meta={'grading_agency': ga.lower(),
                                                                                     'grade': grade.lower(), })

    def parse_item_page(self, response):
        il = ItemLoader(item=CoinItem())

        il.add_value('coin_url', response.url)

        # Source
        source_netloc = urlparse.urlparse(response.url).netloc
        il.add_value('source', source_netloc)

        description_xpath = response.selector.xpath('//span[contains(@itemprop, "description")]')
        raw_description = remove_tags(' '.join(description_xpath.extract()))
        il.add_value('description', raw_description)
        split_description = re.split('; |, |\*|\n|\s|\)|\(', raw_description)

        pcgs_base = None
        pcgs_variety = None
        ngc_id = None

        for i in range(len(split_description) - 2):
            if 'NGC' in split_description[i] and 'ID#' in split_description[i + 1]:
                ngc_id = split_description[i + 2]
            elif 'Variety' in split_description[i] and 'PCGS#' in split_description[i + 1]:
                pcgs_variety = split_description[i + 2]
            elif 'PCGS#' in split_description[i + 1]:
                pcgs_base = split_description[i + 2]

        if pcgs_variety is not None:
            pcgs_id = pcgs_variety
        else:
            pcgs_id = pcgs_base

        il.add_value('pcgs_id', pcgs_id)
        il.add_value('ngc_id', ngc_id)

        # Grading Agency
        for ga in self.GRADING_AGENCIES:
            if ga in response.meta['grading_agency']: #Dont have a good source of this data since im not coming from search page
                il.add_value('grading_agency', ga)


        # Grade
        grade_re = '(' + '|'.join(self.PROOF_GRADES + self.NORMAL_GRADES) + ')(-*\d{2}|[1-9])'
        prelim_grade = re.findall(grade_re, response.meta['grade'])

        try:
            prelim_grade_value = prelim_grade[0][1]
        except IndexError:
            prelim_grade_value = None

        if 'details' in raw_description.lower() or not (prelim_grade) or len(prelim_grade) != 1 or prelim_grade_value is None:
            il.add_value('grade', 'bad grade')
        else:
            il.add_value('grade', prelim_grade_value)

        # Details
        if 'details' in raw_description.lower():
            il.add_value('details', True)
        else:
            il.add_value('details', False)

        # Plus
        if '+' in raw_description or 'plus' in raw_description.lower():
            il.add_value('plus', True)
        else:
            il.add_value('plus', False)

        # CAC
        if 'cac' in raw_description.lower():
            il.add_value('cac', True)
        else:
            il.add_value('cac', False)

        current_price_before_bp = response.selector.xpath('//div[contains(@itemprop,"offers")]/span/strong/text()').extract_first() \
            .replace('$', '') \
            .replace(',', '')
        il.add_value('price', current_price_before_bp)



        # Proof
        try:
            prelim_grade_str = prelim_grade[0][0]
        except IndexError:
            prelim_grade_str = None

        if prelim_grade_str in self.PROOF_GRADES or 'proof' in raw_description.lower():
            il.add_value('proof', True)
        else:
            il.add_value('proof', False)

        img_w_size = response.selector.xpath('//div[contains(@class,"gallery-view-nav")]/a/img/@data-src').extract()
        image_urls = [re.sub(r'sizedata%5B[0-9]{1,4}x[0-9]{1,4}%5D', '', a) for a in img_w_size]
        il.add_value('image_urls', image_urls)

        # uuid
        il.add_value('uuid', uuid.uuid5(uuid.NAMESPACE_URL, response.url).hex)
        return il.load_item()
