import re
import urllib.parse as urlparse
import uuid
import random
from io import BytesIO

import pytesseract
import requests
import scrapy
from PIL import Image
from scrapy.conf import settings
from scrapy.loader import ItemLoader
from w3lib.html import remove_tags
import urllib3
import math

from ebay_spider.items import CoinItem
from ebay_spider.items import PCGSIDItem



class DLPCGSIDSpider(scrapy.Spider):
    name = 'pcgsidcoinfacts'
    NORMAL_GRADES = settings.get('NORMAL_GRADES')
    GRADING_AGENCIES = settings.get('GRADING_AGENCIES')
    PROOF_GRADES = settings.get('PROOF_GRADES')
    pcgs_ids = settings.get('PCGS_IDS')

    def start_requests(self):
        #TODO: custom directory iterator that returns grade and also all hirearchical type information
        # in keras/preprocessing/image.py
        # have
        # def _get_batches_of_transformed_samples(self, index_array):
        # return all y information

        # in class DirectoryIterator(Iterator): --> __init()
        # when i load the filenames also load all the extra coin type information.  Maybe presave this somewhere in a
        # filename -> [type, sub_type, ... year, pcgs_id, sub_pcgs_id] direcotry in a pickle on the disk

        # a = 'https://www.pcgs.com/pop/uscoins'
        half_cents_and_cents = ['https://www.pcgs.com/pop/detail/liberty-cap-half-cent-1793-1797/34',
                                'https://www.pcgs.com/pop/detail/draped-bust-half-cent-1800-1808/653',
                                'https://www.pcgs.com/pop/detail/classic-head-half-cent-1809-1836/654',
                                'https://www.pcgs.com/pop/detail/braided-hair-half-cent-1840-1857/655',
                                'https://www.pcgs.com/pop/detail/flowing-hair-large-cent-1793-1796/38',
                                'https://www.pcgs.com/pop/detail/draped-bust-cent-1796-1807/660',
                                'https://www.pcgs.com/pop/detail/classic-head-cent-1808-1814/661',
                                'https://www.pcgs.com/pop/detail/coronet-head-cent-1816-1839/662',
                                'https://www.pcgs.com/pop/detail/braided-hair-cent-1839-1857/663',
                                'https://www.pcgs.com/pop/detail/flying-eagle-cent-1856-1858/664',
                                'https://www.pcgs.com/pop/detail/indian-cent-1859-1909/44',
                                'https://www.pcgs.com/pop/detail/lincoln-cent-wheat-reverse-1909-1958/46',
                                'https://www.pcgs.com/pop/detail/lincoln-cent-modern-1959-date/47', ]

        two_and_three_cents = ['https://www.pcgs.com/pop/detail/two-cent-1864-1873/670',
                               'https://www.pcgs.com/pop/detail/three-cent-silver-1851-1873/77',
                               'https://www.pcgs.com/pop/detail/three-cent-nickel-1865-1889/671', ]

        nickels = ['https://www.pcgs.com/pop/detail/shield-nickel-1866-1883/81',
                   'https://www.pcgs.com/pop/detail/liberty-nickel-1883-1913/82',
                   'https://www.pcgs.com/pop/detail/buffalo-nickel-1913-1938/83',
                   'https://www.pcgs.com/pop/detail/jefferson-nickel-1938-date/84', ]

        half_dimes_and_dimes = ['https://www.pcgs.com/pop/detail/bust-half-dime-1792/683',
                                'https://www.pcgs.com/pop/detail/flowing-hair-half-dime-1794-1795/684',
                                'https://www.pcgs.com/pop/detail/draped-bust-half-dime-1796-1805/92',
                                'https://www.pcgs.com/pop/detail/capped-bust-half-dime-1829-1837/687',
                                'https://www.pcgs.com/pop/detail/liberty-seated-half-dime-1837-1873/93',
                                'https://www.pcgs.com/pop/detail/draped-bust-dime-1796-1807/1655',
                                'https://www.pcgs.com/pop/detail/capped-bust-dime-1809-1837/94',
                                'https://www.pcgs.com/pop/detail/liberty-seated-dime-1837-1891/95',
                                'https://www.pcgs.com/pop/detail/barber-dime-1892-1916/702',
                                'https://www.pcgs.com/pop/detail/mercury-dime-1916-1945/703',
                                'https://www.pcgs.com/pop/detail/roosevelt-dime-1946-date/98', ]

        quarters = ['https://www.pcgs.com/pop/detail/twenty-cent-1875-1878/705',
                    'https://www.pcgs.com/pop/detail/draped-bust-quarter-1796-1807/1657',
                    'https://www.pcgs.com/pop/detail/capped-bust-quarter-1815-1838/108',
                    'https://www.pcgs.com/pop/detail/liberty-seated-quarter-1838-1891/109',
                    'https://www.pcgs.com/pop/detail/barber-quarter-1892-1916/716',
                    'https://www.pcgs.com/pop/detail/standing-liberty-quarter-1916-1930/111',
                    'https://www.pcgs.com/pop/detail/washington-quarter-1932-1998/112',
                    'https://www.pcgs.com/pop/detail/washington-50-states-quarters-1999-2008/720',
                    'https://www.pcgs.com/pop/detail/washington-d-c-u-s-territories-quarters-2009/1721',
                    'https://www.pcgs.com/pop/detail/washington-america-beautiful-quarters-2010-2021/1715', ]

        half_dollars = ['https://www.pcgs.com/pop/detail/flowing-hair-half-dollar-1794-1795/721',
                        'https://www.pcgs.com/pop/detail/draped-bust-half-dollar-1796-1807/1658',
                        'https://www.pcgs.com/pop/detail/capped-bust-half-dollar-1807-1839/120',
                        'https://www.pcgs.com/pop/detail/liberty-seated-half-dollar-1839-1891/121',
                        'https://www.pcgs.com/pop/detail/barber-half-dollar-1892-1915/732',
                        'https://www.pcgs.com/pop/detail/walking-liberty-half-dollar-1916-1947/733',
                        'https://www.pcgs.com/pop/detail/franklin-half-dollar-1948-1963/734',
                        'https://www.pcgs.com/pop/detail/kennedy-half-dollar-1964-date/125', ]



        dollars = ['https://www.pcgs.com/pop/detail/flowing-hair-dollar-1794-1795/736',
                   'https://www.pcgs.com/pop/detail/draped-bust-dollar-1795-1804/23',
                   'https://www.pcgs.com/pop/detail/liberty-seated-dollar-1836-1873/29',
                   'https://www.pcgs.com/pop/detail/trade-dollar-1873-1885/743',
                   'https://www.pcgs.com/pop/detail/morgan-dollar-1878-1921/744',
                   'https://www.pcgs.com/pop/detail/peace-dollar-1921-1935/26',
                   'https://www.pcgs.com/pop/detail/ike-dollar-1971-1978/31',
                   'https://www.pcgs.com/pop/detail/susan-b-anthony-dollar-1979-1999/747',
                   'https://www.pcgs.com/pop/detail/sacagawea-dollar-2000-date/748',
                   'https://www.pcgs.com/pop/detail/presidential-dollars-2007-date/1650',
                   ]

        gold_coins = ['https://www.pcgs.com/pop/detail/gold-dollar-1849-1889/51',
                      'https://www.pcgs.com/pop/detail/draped-bust-2-5-1796-1807/1656',
                      'https://www.pcgs.com/pop/detail/capped-bust-2-5-1808-1834/52',
                      'https://www.pcgs.com/pop/detail/classic-head-2-5-1834-1839/757',
                      'https://www.pcgs.com/pop/detail/liberty-head-2-5-1840-1907/758',
                      'https://www.pcgs.com/pop/detail/indian-2-5-1908-1929/759',
                      'https://www.pcgs.com/pop/detail/three-dollar-1854-1889/760',
                      'https://www.pcgs.com/pop/detail/4-stella-1879-1880/57',
                      'https://www.pcgs.com/pop/detail/draped-bust-5-1795-1807/58',
                      'https://www.pcgs.com/pop/detail/capped-bust-5-1807-1834/59',
                      'https://www.pcgs.com/pop/detail/classic-head-5-1834-1838/768',
                      'https://www.pcgs.com/pop/detail/liberty-head-5-1839-1908/61',
                      'https://www.pcgs.com/pop/detail/indian-5-1908-1929/771',
                      'https://www.pcgs.com/pop/detail/draped-bust-10-1795-1804/63',
                      'https://www.pcgs.com/pop/detail/liberty-head-10-1838-1907/64',
                      'https://www.pcgs.com/pop/detail/indian-10-1907-1933/65',
                      'https://www.pcgs.com/pop/detail/liberty-head-20-1849-1907/66',
                      'https://www.pcgs.com/pop/detail/st-gaudens-20-1907-1933/67',
                      ]

        commemoratives = ['https://www.pcgs.com/pop/detail/silver-commemorative-1892-1954/789',
                          'https://www.pcgs.com/pop/detail/gold-commemorative-1903-1926/791',
                          'https://www.pcgs.com/pop/detail/modern-silver-clad-commemoratives-1982-date/792',
                          'https://www.pcgs.com/pop/detail/modern-gold-commemorative-1984-date/1647',
                          'https://www.pcgs.com/pop/detail/norse-medal/790',
                          'https://www.pcgs.com/pop/detail/2016-centennial-series/141623',
                          ]

        bullion_coins = ['https://www.pcgs.com/pop/detail/silver-eagles-1986-date/939',
                         'https://www.pcgs.com/pop/detail/5-oz-america-beautiful-silver-quarters-2010-date/1653',
                         'https://www.pcgs.com/pop/detail/gold-eagles-1986-date/793',
                         'https://www.pcgs.com/pop/detail/platinum-eagles-1997-date/940',
                         'https://www.pcgs.com/pop/detail/gold-buffalos-2006-date/1651',
                         'https://www.pcgs.com/pop/detail/first-spouses-2007-date/1652',
                         'https://www.pcgs.com/pop/detail/ultra-high-relief-double-eagles-2009/941',
                         'https://www.pcgs.com/pop/detail/high-relief-100-gold-2015/942',
                         'https://www.pcgs.com/pop/detail/palladium-25-eagle-2017-date/152898',
                         'https://www.pcgs.com/pop/detail/american-liberty-gold-2017-date/155431', ]

        colonials = ['https://www.pcgs.com/pop/detail/counterstamped-coins/156243',
                     'https://www.pcgs.com/pop/detail/massachusetts-silver-coins-1652-1662/5953',
                     'https://www.pcgs.com/pop/detail/pre-1776-states-coinage-1652-1774/5900',
                     'https://www.pcgs.com/pop/detail/pre-1776-private-regional-issues-1616-1766/5901',
                     'https://www.pcgs.com/pop/detail/french-colonies-1670-1767/5912',
                     'https://www.pcgs.com/pop/detail/post-1776-states-coinage-1788/5902',
                     'https://www.pcgs.com/pop/detail/post-1776-private-regional-issues-1778-1820/5903',
                     'https://www.pcgs.com/pop/detail/proposed-national-issues-1776-1787/5904',
                     'https://www.pcgs.com/pop/detail/washington-pieces-1783-1800/5905',
                     'https://www.pcgs.com/pop/detail/libertas-americana-medals-1781/5943',
                     'https://www.pcgs.com/pop/detail/fugio-cents-1787/5954',
                     'https://www.pcgs.com/pop/detail/colonial-restrikes-fantasies/6404', ]

        territorial = ['https://www.pcgs.com/pop/detail/templeton-reid-georgia-1830-1849/847',
                       'https://www.pcgs.com/pop/detail/bechtler-n-carolina-georgia-1831-1850/1682',
                       'https://www.pcgs.com/pop/detail/california-gold-1849-1855/849',
                       'https://www.pcgs.com/pop/detail/california-fractional-gold-1852-1882/1659',
                       'https://www.pcgs.com/pop/detail/oregon-gold-1849/850',
                       'https://www.pcgs.com/pop/detail/mormon-gold-utah-1849-1860/851',
                       'https://www.pcgs.com/pop/detail/colorado-gold-1860-1861/1612',
                       'https://www.pcgs.com/pop/detail/hawaii-1847-1891/919',
                       'https://www.pcgs.com/pop/detail/u-s-philippines-1903-1945/943',
                       'https://www.pcgs.com/pop/detail/alaska-rural-rehabilitation-corp-1935/921',
                       'https://www.pcgs.com/pop/detail/confederate-states-america-1861-1863/920',
                       'https://www.pcgs.com/pop/detail/lesher-colorado-dollars-1900-1901/853', ]

        patterns = ['https://www.pcgs.com/pop/detail/patterns-1792-1859/795',
                    'https://www.pcgs.com/pop/detail/patterns-1860-1865/1624',
                    'https://www.pcgs.com/pop/detail/patterns-1866-1869/1625',
                    'https://www.pcgs.com/pop/detail/patterns-1870/1626',
                    'https://www.pcgs.com/pop/detail/patterns-1871-1873/1627',
                    'https://www.pcgs.com/pop/detail/patterns-1874-1879/1628',
                    'https://www.pcgs.com/pop/detail/patterns-1880-1942/1629',
                    'https://www.pcgs.com/pop/detail/patterns-1943-date/1630',
                    'https://www.pcgs.com/pop/detail/die-trials-hub-splashers-1792-1982/6413',
                    'https://www.pcgs.com/pop/detail/privately-issued-patterns-1792-1938/6414']


        miscellaneous = ['https://www.pcgs.com/pop/detail/u-s-mint-medals/794',
                         'https://www.pcgs.com/pop/detail/feuchtwanger-tokens/918',
                         'https://www.pcgs.com/pop/detail/so-called-dollars/1008',
                         'https://www.pcgs.com/pop/detail/u-s-assay-commission-medals/924',
                         'https://www.pcgs.com/pop/detail/betts-medals-1845-1887/6273',
                         'https://www.pcgs.com/pop/detail/civil-war-tokens-1863-1864/6274',
                         'https://www.pcgs.com/pop/detail/store-cards-1800-date/6275',
                         'https://www.pcgs.com/pop/detail/hard-times-tokens-1828-1848/6284',
                         'https://www.pcgs.com/pop/detail/rulau-tokens-1700-1900/6289',
                         'https://www.pcgs.com/pop/detail/misc-medallic-token-fantasy-coinage/157644',
                         ]

        coin_types = [half_cents_and_cents, two_and_three_cents, nickels, half_dimes_and_dimes, quarters, half_dollars,
                      dollars, gold_coins, commemoratives, bullion_coins, colonials, territorial, patterns,
                      miscellaneous]

        coin_type_strings = ['half_cents_and_cents', 'two_and_three_cents', 'nickels', 'half_dimes_and_dimes',
                             'quarters', 'half_dollars', 'dollars', 'gold_coins', 'commemoratives', 'bullion_coins',
                             'colonials', 'territorial', 'patterns', 'miscellaneous']

        for coin_type_list, coin_type_string in zip(coin_types, coin_type_strings):
            for sub_coin_type_url in coin_type_list:
                sub_coin_type_id = sub_coin_type_url.split('/')[-1]
                for proof_or_not in ['MS', 'PR']:
                    if proof_or_not == 'MS':
                        this_proof = False
                    else:
                        this_proof = True
                    this_url = sub_coin_type_url + ('/0?t=5&p=%s&pn=1&ps=-1' % proof_or_not)
                    yield scrapy.Request(url=this_url, callback=self.parse_population_page,
                                         meta={'coin_type': coin_type_string, 'sub_coin_type': sub_coin_type_id,
                                               'proof': this_proof})


    def parse_population_page(self, response):

        sub_sub_coin_type_items = response.xpath('//div[contains(@class, "text-bold text-large")]')
        for item in sub_sub_coin_type_items:
            sub_sub_coin_type = item.xpath('text()').extract_first().replace(', MS', '').replace(', PR', '')
            print(sub_sub_coin_type)

        #     il.add_value('coin_type', response.meta['coin_type'])
        #     il.add_value('sub_coin_type', response.meta['sub_coin_type'])
        #     il.add_value('sub_sub_coin_type', sub_sub_coin_type)

        #     il.add_value('proof', response.meta['proof'])

        next = self.driver.find_element_by_xpath('//*[@id="BTN_NEXT"]')
        url = 'http://www.example.org/abcd'
        yield Request(url, callback=self.parse2)
        next.click()

        # for [MS, Proof]:
        #   go to comprehensive page
        #   for sub_sub_coin_type:
        #       click to open all sub_pcgs_ids (if can not open set sub_pcgs_id to 'base')
        #       get description
        #       find year if possible or set to 'unknown'
        #       get pcgs_id
        #       get sub_pcgs_id
        #       get population list
        #       check population key is what I expect
        #       generate uuid

        pcgs_id = response.url.split('/')[-1]
        table_columns = response.xpath('//div[@class="valueview-content"]/div/div/div/div[contains(@id, "grade")]')
        for table_column in table_columns:
            grade = table_column.xpath('div[@class="highlight grade-name"]/text()').extract_first()
            grade_only = grade.replace("+", "")

            plus = False
            if "+" in grade:
                plus = True

            pcgs_guide_price = table_column.xpath('div[@class="grade-price"]/text()').extract_first()
            if pcgs_guide_price is not None:
                pcgs_guide_price_fixed = int(pcgs_guide_price.replace("$", "").replace(",", ""))
            else:
                pcgs_guide_price_fixed = 0
            grade_pop = table_column.xpath('div[@class="grade-pop"]/text()').extract_first()

            il = ItemLoader(item=PCGSIDItem())
            il.add_value('price', pcgs_guide_price_fixed)
            il.add_value('pcgs_id', pcgs_id)
            il.add_value('grading_agency', 'pcgs')
            il.add_value('grade', grade_only)
            il.add_value('plus', plus)
            il.add_value('type', 'guide')
            il.add_value('grade_pop', grade_pop)


            yield il.load_item()
            # print(il.load_item())

            rows = table_column.xpath('div')

            counter = 0
            for row in rows:
                if "grade-title" in row.extract():
                    counter += 1
                if counter == 3:
                    agency = 'pcgs'
                elif counter == 4:
                    agency = 'ngc'
                else:
                    agency = None

                if agency is not None and "grade-auction" in row.extract():
                    value = row.xpath('a/span[@class="float-right "]/text()').extract_first()
                    if value:
                        price = int(value.replace("$", "").replace(",", ""))
                        il = ItemLoader(item=PCGSIDItem())
                        il.add_value('price', price)
                        il.add_value('pcgs_id', pcgs_id)
                        il.add_value('grading_agency', agency)
                        il.add_value('grade', grade_only)
                        il.add_value('plus', plus)
                        il.add_value('type', 'auction')
                        il.add_value('grade_pop', grade_pop)
                        yield il.load_item()
                        # if grade_only == '66' and price == 15:
                        #     this_item = il.load_item()
                        #     print(il.load_item())


