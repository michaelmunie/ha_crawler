# -*- coding: utf-8 -*-

from scrapy import signals
from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy.exporters import JsonLinesItemExporter
from PIL import Image
from io import BytesIO
import requests
import urllib3
import random

from time import sleep


def logical_xor(str1, str2):
    return bool(str1) ^ bool(str2)


class DropItemSource(DropItem):
    pass


class DropItemCoinID(DropItem):
    pass


class DropItemGradingAgency(DropItem):
    pass


class DropItemType(DropItem):
    pass


class DropItemGrade(DropItem):
    pass


class DropItemPrice(DropItem):
    pass


class DropItemImageUrls(DropItem):
    pass


class DropItemImageDownload(DropItem):
    pass


class EbaySpiderPipeline(object):
    NORMAL_GRADES = settings.get('NORMAL_GRADES')
    GRADING_AGENCIES = settings.get('GRADING_AGENCIES')
    PROOF_GRADES = settings.get('PROOF_GRADES')
    # DOWNLOAD_LOCATION = settings.get('DOWNLOAD_LOCATION')
    
    def __init__(self, download_location):
        urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
        self.files = {}
        self.download_location = download_location

    @classmethod
    def from_crawler(cls, crawler):
        try:
            pipeline = cls(download_location=crawler.spider.download_location)
        except AttributeError:
            try:
                pipeline = cls(download_location='/home/munie/Desktop/')
            except AttributeError:
                pipeline = cls(download_location='/home/ubuntu/temp/')

        # pipeline = cls(download_location=crawler.spider.download_location)
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline
    
    def spider_opened(self, spider):
        file = open('%s_products.jl' % spider.name, 'w+b')
        self.files[spider] = file
        self.exporter = JsonLinesItemExporter(file)
        self.exporter.start_exporting()
    
    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_coins_ha_item(self, item, spider):
        # spider.logger.debug("")
        # try:
        if not item['source']:
            raise DropItemSource("Missing source in %s" % item)
        elif (not item.get('pcgs_id', False) and not item.get('ngc_id', False)):
            raise DropItemCoinID("Missing coin id in %s" % item)
        elif not (item['grading_agency']):
            raise DropItemGradingAgency("Missing grading agency in %s" % item)
        elif not (item['image_urls']):
            raise DropItemImageUrls("No image urls found in %s" % item)
        elif len(item['image_urls']) < 2:
            raise DropItemImageUrls("Less than two image urls found in %s" % item)

        try:
            grade = abs(int(item['grade'].strip()))
            if grade > 0 and grade <= 70:
                item['grade'] = str(grade)
            elif item['details'] is True:
                pass # leave this item alone, since it'll be suffixed with .details anyways
            else:
                raise DropItemGrade("item grade not details and not between 1 and 70 in %s" % item)
        except Exception:
            if item['details'] is True:
                pass # leave this item alone, since it'll be suffixed with .details anyways
            else:
                raise DropItemGrade("problem reading item grade in %s" % item)



        if settings.get('DOWNLOAD_IMAGES'):
            for (i, img_url) in enumerate(item['image_urls'], 1):

                if item.get('pcgs_id', None) is not None:
                    coin_id = 'pcgs_id_' + item['pcgs_id']
                elif item.get('ngc_id', None) is not None:
                    coin_id = 'ngc_id_' + item['ngc_id']
                else:
                    raise DropItemCoinID("Missing pcgs coin id in %s" % item)

                if item['details'] is True:
                    details_suffix = '.details'
                else:
                    details_suffix = ''

                success = False
                for tries in range(3):
                    if success:
                        break

                    r = requests.get(img_url,
                                     proxies=random.choice(settings.get('PROXIES')),
                                     allow_redirects=True)

                    if len(r.content) >= 50000:  # even sucessful responses give a 404, is this a stone-age site protection?
                        img = Image.open(BytesIO(r.content))
                        suffix = img.format.lower()

                        if suffix == 'jpeg' or suffix == 'jpg':
                            filename_type = 'jpeg'
                        else:
                            filename_type = 'png'

                        filename = '%s_%s_image_%i_%s.%s%s' % (coin_id, str(item['uuid']), i, suffix, filename_type, details_suffix)
                        img = img.convert('RGB')
                        img.save(self.download_location + filename, filename_type)
                        success = True
                    else:
                        extra = 1
                        if r.status_code == 503:
                            extra = 188 # about 600 / (.5 + 1 + 2)
                        sleep(extra * 0.5 * (2 ** tries))

                if success is False:
                    raise DropItemImageDownload("coin image download failure in %s" % item)

        self.exporter.export_item(item)
        return item

    def process_item(self, item, spider):
        if spider.name == 'ha_coins_buy':
            return self.process_coins_ha_item(item, spider)
        else:
            raise NotImplementedError
